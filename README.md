# Cheat Engine Health Pointer Challenge

Try to find a pointer to the health value, without using the encrypted value, the location nor the xor_value,
these are there just for debugging reasons.\
Also solve it without the knowledge of this code.\
This code is just here to compile it yourself.

## Building the challenge from source

You can either use cmake or use g++ directly.\
I downloaded mingw64 as my compiler.\
```g++.exe -static -std=c++11 main.cpp -o challenge.exe```\

## Downloading the challenge

If you are brave enough and trust me, here is the download link for the [challenge](https://gofile.io/?c=n4cG0a) :)
