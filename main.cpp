#include <iostream>
#include <ctime>
#include <cstdlib>
#include <iomanip>

using namespace std;

typedef struct {
    unsigned char health;
} enemy;

unsigned char xor_value = 0;

void printRules();

int obfuscate(int i) {
    return _rotl(i, 1) xor xor_value;
}

int deobfuscate(int i) {
    return _rotr(i xor xor_value, 1);
}

int main() {

    srand(time(0));
    xor_value = rand();
    auto e1 = (enemy *) malloc(sizeof(enemy));
    e1->health = obfuscate(100);

    printRules();

    while (true) {
        auto random_number = (rand() % 10) + 1;
        for (int i = 0; i < random_number; i++)
            malloc(random_number); // waste space, to get a new address for the enemy object

        auto tmp = (enemy *) malloc(sizeof(enemy));
        tmp->health = obfuscate(deobfuscate(e1->health) - 1);
        free(e1);
        e1 = tmp;

        if (deobfuscate(e1->health) > 100) {
            cout << "You found the health value!" << endl;
            return 0;
        }

        if (deobfuscate(e1->health) < 1) {
            cout << "You lost, try again!" << endl;
            return 1;
        }

        cout <<
             setfill('0') << setw(2) << dec << (int) deobfuscate(e1->health) << " | " <<
             setfill('0') << setw(3) << dec << (int) e1->health << " | " <<
             e1 << endl;

        cin.get();
    }
}

void printRules() {
    cout
            << "Try to find a pointer to the health value, without using the encrypted value, the location nor the xor_value, "
            << endl <<
            "these are there just for debugging reasons. Also solve it without the knowledge of this code."
            << endl <<
            "This code is just here to compile it yourself." << endl << endl;
    cout << "Press enter to take 1 damage." << endl;
    cout << "xor_value in hex: " << setw(2) << setfill('0') << hex << (int) xor_value << endl << endl;
    cout << "health value | encrypted health value | health location" << endl;
}
